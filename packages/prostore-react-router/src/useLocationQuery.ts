import { useMemo } from 'react';
import {
  StoreOrName,
  useContextStore,
  useObservableState
} from '@proscom/prostore-react';
import { LocationStore } from './LocationStore';
import { ITransformedQuery } from './transformers';

/**
 * Хук, позволяющий подписаться на часть квери-параметров
 * @param storeOrName - LocationStore или его имя в ProstoreContext
 * @param keys - массив имен квери-параметров, на которые надо подписаться
 * @returns [state, store], где
 *  state - трансформированные значения квери-параметров,
 *  store - LocationStore
 */
export function useLocationQuery<
  Params extends ITransformedQuery = ITransformedQuery,
  Keys extends readonly string[] = readonly string[]
>(storeOrName: StoreOrName<LocationStore>, keys: Keys) {
  const locationStore = useContextStore(storeOrName);
  const query$ = useMemo(() => {
    return locationStore.get$<Params, Keys>(...keys);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [locationStore, ...keys]);
  return [useObservableState(query$, query$.value), locationStore] as const;
}
