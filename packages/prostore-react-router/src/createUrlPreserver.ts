import { format as formatUrl, parse as parseUrl } from 'url';
import { parse as qsParseQuery, stringify as stringifyQuery } from 'query-string';
import {
  IQueryTransformers,
  ITransformedQuery,
  stringifyQueryWithTransformers
} from './transformers';

function parseQuery(query: string|null|undefined) {
  // Функция qsParseQuery на самом деле может принимать false-значение не string
  return qsParseQuery(query as any);
}

/**
 * Создает конструктор ссылок, сохраняющих набор параметров адресной строки
 *
 * @param preservedQuery - набор десериализованных квери-параметров для сохранения
 * @param transformers - набор трансформеров для сериализации квери-параметров
 */
export function createUrlPreserver(
  preservedQuery: ITransformedQuery,
  transformers?: IQueryTransformers
) {
  const preservedQueryRaw = stringifyQueryWithTransformers(
    preservedQuery,
    transformers
  );

  /**
   * Создает ссылку с сохранением query-параметров.
   * Прогоняет query-параметры через сериализаторы из набора трансформеров
   *
   * @param pathname - адрес для перехода
   * @param newQuery - новые квери-параметры
   */
  function createUrl(pathname: string, newQuery?: ITransformedQuery) {
    const parsedUrl = parseUrl(pathname);
    const parsedQueryRaw = parseQuery(parsedUrl.search);

    const newQueryRaw = stringifyQueryWithTransformers(newQuery, transformers);

    const query = {
      ...preservedQueryRaw,
      ...parsedQueryRaw,
      ...newQueryRaw
    };

    parsedUrl.search = stringifyQuery(query);

    return formatUrl(parsedUrl);
  }

  return createUrl;
}
