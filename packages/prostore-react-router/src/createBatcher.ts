export function createBatcher<Args extends any[], Data>(
  mapper: (data: Data | null, ...args: Args) => Data,
  executor: (data: Data) => void
) {
  let timeoutId: number | null = null;
  let data: Data | null = null;

  return (...args: Args) => {
    data = mapper(data, ...args);
    if (!timeoutId) {
      timeoutId = (setTimeout(() => {
        timeoutId = null;
        const cdata = data!;
        data = null;
        executor(cdata);
      }, 0) as any) as number;
    }
  };
}
