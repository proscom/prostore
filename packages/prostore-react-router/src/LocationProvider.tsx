import React, { useLayoutEffect, useRef, useState } from 'react';
import { unstable_batchedUpdates } from 'react-dom';
import { StoreOrName, useContextStore } from '@proscom/prostore-react';
import {
  __RouterContext as RouterContext,
  RouteComponentProps
} from 'react-router';
import { useReactRouter } from './useReactRouter';
import { LocationStore } from './LocationStore';

export interface LocationProviderProps {
  children?: React.ReactNode;
  storeOrName: StoreOrName<LocationStore>;
}

/**
 * Компонент, который синхронизирует LocationStore и контекст react-router
 */
export function LocationProvider({
  children,
  storeOrName
}: LocationProviderProps) {
  // location -> RouterContext -> LocationProvider -> LocationStore -> syncedContext

  // Подписываемся на изменения от react-router, получаем контекст роутера
  const routerContext = useReactRouter();

  // Инжектируем наш LocationStore
  const locationStore = useContextStore(storeOrName);

  // Когда меняется наш стор, обновляем сохраненный контекст роутера
  // Таким образом контекст роутера синхронизирован с нашим стором
  // При этом все подписчики locationStore в том числе этот
  // получают обновления синхронно, поэтому реакт собирает их в кучу
  // и оптимизирует

  // Место для хранения синхронизированного контекста
  const [
    syncedContext,
    setSyncedContext
  ] = useState<RouteComponentProps | null>(null);

  // При обновлении location в react-router, меняем его в нашем сторе
  const { location } = routerContext;
  useLayoutEffect(() => {
    unstable_batchedUpdates(() => {
      locationStore.updateLocation(location);
    });
  }, [locationStore, location]);

  // Запоминаем последний актуальный контекст
  const routerContextRef = useRef(routerContext);
  routerContextRef.current = routerContext;

  // При обновлении нашего стора синхронизируем контекст
  useLayoutEffect(() => {
    const subscription = locationStore.state$.subscribe(() => {
      setSyncedContext(routerContextRef.current);
    });
    return () => subscription.unsubscribe();
  }, [locationStore.state$]);

  if (!syncedContext) return null;

  return (
    <RouterContext.Provider value={syncedContext}>
      {children}
    </RouterContext.Provider>
  );
}
