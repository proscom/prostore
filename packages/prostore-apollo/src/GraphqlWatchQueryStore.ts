import { from, Observable } from 'rxjs';
import { switchMap } from "rxjs/operators";
import {
  IRequestStoreOptions,
  IRequestStoreParams,
  RequestStore
} from '@proscom/prostore';
import ApolloClient, { WatchQueryOptions } from 'apollo-client';
import { DocumentNode } from 'graphql';
import { IMapData } from '@proscom/prostore';

export type ApolloWatchQueryOptions<Vars> = Omit<
  WatchQueryOptions<Vars>,
  'query' | 'variables'
>;

export interface IGraphqlWatchQueryOptions<Vars, Data> extends IRequestStoreOptions<Vars, Data> {
  /** Additional Apollo query options */
  apolloOptions: ApolloWatchQueryOptions<Vars>;
}

export interface IGraphqlWatchQueryStoreParams<Vars, Data>
  extends IRequestStoreParams<Vars, Data> {
  /**
   * GraphQL query definition using graphql AST from the graphql-tag packages
   *
   * (be sure to include ids wherever possible in order for Apoolo cache to work correctly)
   */
  query: DocumentNode;

  /** Apollo client to be used for fetching the data */
  client: ApolloClient<any>;

  /**
   * Optional callback to transform the query result into the request store state.
   * Use it to remove unnecessary nesting or transform the data
   */
  mapData?: IMapData<Data>;

  /** Additional Apollo query options */
  apolloOptions?: ApolloWatchQueryOptions<Vars>;
}

/**
 * RequestStore tailored to make Apollo GraphQL requests.
 * It also subscribes to Apollo Cache by using watchQuery
 */
export class GraphqlWatchQueryStore<Vars, Data> extends RequestStore<
  Vars,
  Data,
  IGraphqlWatchQueryOptions<Vars, Data>
> {
  query: DocumentNode;
  client: ApolloClient<any>;
  mapData: IMapData<Data>;
  apolloOptions: ApolloWatchQueryOptions<Vars>;

  constructor({
    query,
    client,
    mapData = (x) => x,
    apolloOptions = {},
    ...requestProps
  }: IGraphqlWatchQueryStoreParams<Vars, Data>) {
    super(requestProps);
    this.query = query;
    this.client = client;
    this.apolloOptions = apolloOptions;
    this.mapData = mapData;

    console.assert(
      query,
      'Parameter "query" is required for GraphqlWatchQueryStore'
    );
    console.assert(
      client,
      'Parameter "client" is required for GraphqlWatchQueryStore'
    );
  }

  /**
   * Performs actual GraphQL request using the Apollo Client
   *
   * @param variables - query variables
   * @param options - additional query options
   */
  performRequest(
    variables: Vars,
    options: IGraphqlWatchQueryOptions<Vars, Data>
  ): Observable<Data> {
    const queryParams: WatchQueryOptions<Vars> = {
      query: this.query,
      variables,
      ...this.apolloOptions,
      ...options.apolloOptions
    };

    const wq = this.client.watchQuery(queryParams);

    return from(wq as any).pipe(switchMap(async (result: any) => await this.mapData(result.data)));
  }
}
