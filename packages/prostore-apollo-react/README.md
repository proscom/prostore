# `prostore-apollo-react`

Адаптер для
[prostore](https://gitlab.com/proscom/prostore/-/tree/master/packages%2Fprostore)
, связывающий
[prostore-apollo](https://gitlab.com/proscom/prostore/-/tree/master/packages%2Fprostore-apollo)
и
[prostore-react](https://gitlab.com/proscom/prostore/-/tree/master/packages%2Fprostore-react).

Позволяет реактивно подключать результат GraphQL запроса в React-компоненты, аналогично
хукам из `react-apollo`, но по единой схеме с другими частями `@proscom/prostore`.

Содержит две полезных фичи:

- `ApolloClientsContext` - прокидывание клиента Apollo через контекст
- `useGraphqlQuery` / `useGraphqlWatchQuery` - хуки, которые
  динамически создают GraphqlQueryStore в соответствии с переданными
  параметрами и подписывают на него компонент.

## ApolloClientsContext

Позволяет прокидывать клиент аполло через контекст реакта.
Таким образом реализуется принцип Dependency Injection, и
снижается связанность кода.

```tsx
// index.ts
import { ApolloClient } from 'apollo-client';
import {
  ApolloClientsContext,
  ApolloClientsManager
} from '@proscom/prostore-apollo-react';
import { App } from './App';

const apolloClient = new ApolloClient(/* ... */);
const apolloClient2 = new ApolloClient(/* ... */);

const apolloContext = new ApolloClientsManager({
  default: apolloClient,
  specific: apolloClient2
});

const rootElement = document.getElementById('root');

ReactDOM.render(
  <ApolloClientsContext.Provider value={apolloContext}>
    <App />
  </ApolloClientsContext.Provider>,
  rootElement
);
```

```tsx
// App.ts
function App() {
  const defaultClient = useContextApolloClient();
  const specificClient = useContextApolloClient('specific');

  // defaultClient === apolloClient
  // specificClient === apolloClient2

  // ...
}
```

## `useGraphqlQuery` / `useGraphqlWatchQuery`

Эти хуки динамически создают стор GraphqlQueryStore / GraphqlWatchQueryStore
в соответствии с параметрами конструктора, переданными в queryOptions,
и подписывают компонент на этот стор с помощью
[`useRequestStore`](https://gitlab.com/proscom/prostore/-/tree/master/packages%2Fprostore-react#useRequestStore)
.
Данные хуки позволяют компоненту при маунте выполнить GraphQL-запрос с помощью Apollo,
и перерендерить компонент при получении результата. Запрос будет автоматически перевыполнен,
если компонент перерендерится с измененными переменными запроса.

`useGraphqlQuery` и `useGraphqlWatchQuery` отличаются только поведением.
API у них одинаковое. `useGraphqlWatchQuery` использует `watchQuery`
из Apollo. Таким образом он подписывается на последующие обновления
кеша Apollo после первоначального выполнения запроса.
Кеш Apollo обновляется например при рефетче после мутаций или
при ручном удалении из него каких-то данных.

```tsx
// Задаем graphql-запрос
const query = gql`
  query getUser($id: ID) {
    user(id: $id) {
      id
      name
    }
  }
`;

// Определяет тип переменных запроса
interface UserQueryVars {
  id: number;
}

// Определяет тип результата запроса
interface UserQueryData {
  name: string;
}

// Это по-сути аргумент конструктора GraphqlQueryStore
// должен быть за пределами компонента (или в useMemo),
// чтобы сохранять ссылочную постоянность, т.к. стор
// будет пересоздаваться при каждом изменении queryOptions
const queryOptions: IUseGraphqlQueryOptions<UserQueryVars, UserQueryData> = {
  query,
  mapData: (result: any) => result.user,
  skipQuery: skipIfNull(null)
};

function UserInfo({ id }: { id?: number }) {
  const userQuery = useGraphqlWatchQuery({
    queryOptions,
    // Сравниваются глубоко с помощью lodash.isEqual
    // если меняются, то запрос сам перевыполняется.
    // Если null, то из-за задания `skipIfNull` выше, запрос не выполняется
    variables: id ? { id } : null,
    // Можно передать конкретный клиент Apollo, либо его ключ,
    // тогда он будет подключен из контекста.
    // Если не передан, то используется клиент по-умолчанию (default)
    client,
    // Дополнительные опции передаются в GraphqlWatchQueryStore.loadData
    options
  });
  const userData = userQuery.state.data;

  // С userQuery можно работать точно так же, как
  // с результатом вызова хука useRequestStore

  if (userQuery.check.spinner) {
    return <Spinner />;
  } else if (userQuery.check.error) {
    return <ErrorMessage error={userQuery.state.error} />;
  } else if (!userData) {
    return 'Not found';
  }

  return <div>{userData.name}</div>;
}
```

Так как `useGraphqlQuery` / `useGraphqlWatchQuery` создают
стор внутри компонента, то при ремаунте компонента стор будет
создан заново. Это не приводит к повторным запросам на бекенд,
так как результат запроса уже сохранен в кеше Apollo.

## `useGraphqlMutation`

Хуки `useGraphqlQuery` / `useGraphqlWatchQuery` представляют собой
реактивную зависимость результата запроса от переменных (параметров запроса).
Это значит, что момент выполнения запроса определяется автоматически.
Такая семантика подходит только запросов, удовлетворяющим свойствам чистой функции,
например для GraphQL Query, возвращающих одни и те же значения для одних и тех же переменных
и не выполняющим побочных эффектов

Мутирующие запросы (Graphql Mutation) следует вызывать в коде императивно
в нужный момент (например, в ответ на клик пользователя по кнопке) с помощью функций
`apollo-client`.
Мутирующий запрос может не оказывать никакого влияния на состояние компонентов, тогда
его можно вызывать просто напрямую без использования хуков из этой библиотеки.

Если же возникает потребность отслеживать статус выполнения мутирующего запроса, то
для этого можно использовать хук `useGraphqlMutation`:

```tsx
import { useGraphqlMutation } from '@proscom/prostore-apollo-react';
import gql from 'graphql-tag';

const mutation = gql`
  mutation saveData($data: Data) {
    saveData(data: $data)
  }
`;

interface Data {
  name: string;
}

interface SaveDataVariables {
  data?: Data;
}

interface SaveDataResult {
  saveData: boolean;
}

function MyComponent() {
  const saveOp = useGraphqlMutation<SaveDataResult, SaveDataVariables>(
    null,
    {
      mutation
    },
    {
      // После завершения операции ставит finished=true на 5 секунд
      // Передайте Infinity, чтобы поставить finished=true навсегда после первого выполнения операции
      finishedTimeout: 5000,
      // Предотвращает повторный вызов операции до завершения предыдущей
      singleton: true
    }
  );

  const {
    // Функция для вызова операции
    run,
    // true, если операция выполняется
    loading,
    // true, если операция недавно завершена
    finished,
    // позволяет поменять значение finished в сложных случаях
    setFinished
  } = saveOp;

  const handleClick = () => {
    run({
      name: 'Boris'
    });
  };

  return (
    <button onClick={handleClick} disabled={loading}>
      {finished ? 'Saved' : loading ? 'Saving...' : 'Save'}
    </button>
  );
}
```

## Рецепты использования

### Пагинация с дозагрузкой

В случаях когда нужна пагинация с дозагрузкой (например, бесконечный скролл с дозагрузкой)
можно использовать функцию `updateData`.

Пример:

```tsx
import { insertPaginatedSlice } from '@proscom/prostore';
import {
  IUseGraphqlQueryOptions,
  useGraphqlQuery
} from '@proscom/prostore-apollo-react';

// Задаем graphql-запрос
const query = gql`
  query getNews($page: Int, $perPage: Int) {
    news(input: { pagination: { page: $page, perPage: $perPage } }) {
      id
      name
    }
  }
`;

interface NewsQueryVars {
  page?: number;
  perPage?: number;
}

interface NewsItem {
  id: string;
  name: string;
}

type NewsQueryData = NewsItem[];

const queryOptions: IUseGraphqlQueryOptions<NewsQueryVars, NewsQueryData> = {
  query,
  mapData: (result) => result.news,
  // Берем старые данные и соединяем с новыми в один большой массив
  updateData: (data, oldData, params) => {
    const page = params.variables.page;
    const perPage = params.variables.perPage;
    return insertPaginatedSlice(data, oldData, page, perPage);
  }
};

function MyComponent() {
  const [page, setPage] = useState(0);

  const query = useGraphqlQuery({
    queryOptions,
    variables: {
      page,
      perPage: 10
    }
  });

  const handleLoadMore = () => {
    setPage((p) => p + 1);
  };

  // Не обязательно использовать query.check для определения того,
  // что рендерить. Можно создать свою комбинацию условий на основе
  // query.state

  return (
    <div>
      {query.state.data?.map((item, iItem) => {
        return <div key={iItem}>{item.name}</div>;
      })}
      {query.state.error && 'Error: ' + query.state.error}
      {query.state.loading ? (
        'Loading...'
      ) : (
        <button onClick={handleLoadMore}>Load More</button>
      )}
    </div>
  );
}
```

[См. пример в CodeSandbox](https://codesandbox.io/s/prostore-react-demo-load-more-elds8?file=/src/App.tsx)
