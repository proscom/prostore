import { combineLatest, Observable, Subscribable } from 'rxjs';
import { auditTime, withLatestFrom } from 'rxjs/operators';

export type Subscribables<Args> = {
  [key in keyof Args]: Subscribable<Args[key]>;
};

/**
 * Subscribes to multiple observables with `combineLatest` and injects
 * latest values from other observables without subscribing to them.
 *
 * Whenever any of the observables from the `sources` changes, the resulting
 * observable emits new values which contains latest values from `sources` and
 * `latest` observable arrays.
 *
 * Subscription to `sources` is asynchronous so that if multiple
 * observables from `sources` change, only one new value is emitted from the result.
 *
 * @param sources - array of observables to subscribe to
 * @param latest - array of other observables whose latest values are injected
 *  into resulting observable
 *
 * @return result - observable whose value is an array.
 *  First element of this array is an array of `sources` values.
 *  Other elements of this array are injected values from `latest` observables.
 *
 * @example
 * const obs1$ = new BehaviorSubject(1);
 * const obs2$ = new BehaviorSubject(2);
 * const obs3$ = new BehaviorSubject(3);
 *
 * const obs$ = combineLatestWith([obs1$, obs2$], [obs3$]);
 * obs$.subscribe(([[v1, v2], v3]) => console.log('emit', v1, v2, v3));
 * await timeout(0);
 * // emit 1 2 3
 * obs3$.next(4);
 * await timeout (0);
 * // no emit
 * obs1$.next(5);
 * obs2$.next(6);
 * await timeout(0);
 * // emit 5 6 4
 */
export function combineLatestWith<
  SArgs extends readonly any[],
  LArgs extends readonly any[]
>(sources: Subscribables<SArgs>, latest: Subscribables<LArgs>) {
  return (combineLatest(sources).pipe(
    auditTime(0),
    withLatestFrom(...latest)
  ) as any) as Observable<[SArgs, ...LArgs]>;
}
