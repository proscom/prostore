/** Function to transform the data to desired output */
export type IMapData<Data> = (result: any) => Data | Promise<Data>;
