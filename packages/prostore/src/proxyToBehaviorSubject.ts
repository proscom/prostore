import { BehaviorSubject, Observable } from "rxjs";

/**
 * Wraps Observable in the BehaviorSubject compatible interface by
 * proxying BehaviorSubject-related calls to the passed subject$.
 *
 * @param obs$ - observable to wrap
 * @param subject$ - subject to which calls are proxied
 */
export function proxyToBehaviorSubject<T>(obs$: Observable<T>, subject$: BehaviorSubject<T>) {
  Object.defineProperties(obs$, {
    value: {
      get: () => subject$.value
    },
    getValue: {
      value: () => subject$.getValue()
    },
    next: {
      value: (data: T) => subject$.next(data)
    }
  });

  return obs$ as BehaviorSubject<T>;
}
