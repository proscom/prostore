import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import {
  initialRequestState,
  IRequestState,
  IRequestStoreOptions,
  IRxRequestStoreParams, IVariablesObservable,
  IVariablesSubjectType,
  RxRequestStore
} from "./RxRequestStore";
import { CustomError } from "@proscom/ui-utils";

export interface IRequestStoreParams<Vars, Data, Options = any>
  extends IRxRequestStoreParams<Vars, Data, Options> {}

export class RequestStoreReactivityError extends CustomError {
}

export class RequestStore<
  Vars = any,
  Data = any,
  Options extends IRequestStoreOptions<Vars, Data> = any
> extends RxRequestStore<Vars, Data, Options> {
  variables$ = new Subject<IVariablesSubjectType<Vars, Options>>();
  isReactive: boolean = false;

  constructor({
    initialData = null,
    skipQuery = (x) => undefined,
    ssrId = null,
    updateData = (data) => data,
    ssrContext = null,
    variables$
  }: IRequestStoreParams<Vars, Data> = {}) {
    super({
      initialData,
      skipQuery,
      ssrId,
      updateData,
      ssrContext,
      variables$
    });
    this.isReactive = !!variables$;
    if (!variables$) {
      this.variables$$.next(this.variables$);
    }
  }

  setVariables$(variables$: IVariablesObservable<Vars, Options> | null) {
    if (variables$) {
      super.setVariables$(variables$);
      this.isReactive = true;
    } else {
      super.setVariables$(this.variables$);
      this.isReactive = false;
    }
  }

  async loadData(
    variables: Vars,
    options?: Options
  ): Promise<IRequestState<Vars, Data>> {
    if (this.isReactive) {
      throw new RequestStoreReactivityError('RequestStoreReactivityError: Cannot use loadData when "variables$" are supplied as an Observable');
    }

    const promise = this.state$
      .pipe(
        first(
          (state) =>
            !state.loading &&
            (state.loaded || state.error) &&
            state.variables === variables
        )
      )
      .toPromise();
    this.variables$.next({ variables, options: options });
    this.registerSsrPromise(promise);
    return await promise;
  }

  /**
   * @deprecated Use skipQuery and change variables instead
   */
  reset(state: Partial<IRequestState<Vars, Data>>) {
    this.state = {
      ...initialRequestState,
      ...state
    };
  }
}

export type GetRequestStoreVars<Store> = Store extends RequestStore<
  infer Vars,
  any,
  any
>
  ? Vars
  : never;
export type GetRequestStoreData<Store> = Store extends RequestStore<
  any,
  infer Data,
  any
>
  ? Data
  : never;
export type GetRequestStoreOptions<Store> = Store extends RequestStore<
  any,
  any,
  infer Options
>
  ? Options
  : never;
export type GetRequestStoreState<Store> = IRequestState<
  GetRequestStoreVars<Store>,
  GetRequestStoreData<Store>
>;
