import { Observable, Subscribable } from 'rxjs';

/**
 * Observable operator which calls the provided callbacks when observable
 * is subscribed to or unsubscribed from
 *
 * @param onSubscribe - function which is called when observable
 *  is subscribed to
 * @param onUnsubscribe - function which is called when observable
 *  is unsubscribed from
 */
export function onSubscription<T>(
  onSubscribe: (() => void) | undefined,
  onUnsubscribe: (() => void) | undefined
) {
  return (source: Subscribable<T>) => {
    return new Observable<T>((observer) => {
      onSubscribe?.();
      const sub = source.subscribe(observer);
      return () => {
        sub.unsubscribe();
        onUnsubscribe?.();
      };
    });
  };
}
