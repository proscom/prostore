import {
  attachValueToObservable,
  BehaviorStore,
  ObservableWithValue
} from '@proscom/prostore';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { tryParseJson } from '@proscom/ui-utils';

/**
 * Состояние LocalStorageStore.
 * Содержит десериализованные значения заданных ключей
 */
export type ILocalStorageState = {
  [key: string]: any;
};

/**
 * Стор, связывающий localStorage и prostore
 *
 * @deprecated Используйте WebStorageValueStore
 */
export class LocalStorageStore extends BehaviorStore<ILocalStorageState> {
  // Набор ключей, которые должны быть прочитаны и на которые необходимо подписаться
  items: string[] = [];

  // Ссылка на localStorage
  localStorage: Storage;

  constructor(localStorage: Storage, keys: string[]) {
    super({});

    this.items = keys;
    this.localStorage = localStorage;
    this.readStorage();
  }

  /**
   * Подписывается на событие window.storage для отслеживания изменений
   * localStorage
   */
  registerListener() {
    window.addEventListener('storage', this.handleStorageChange);
  }

  /**
   * Отписывается от события window.storage
   */
  unregisterListener() {
    window.removeEventListener('storage', this.handleStorageChange);
  }

  /**
   * Читает и десериализует значения ключей из localStorage.
   * Вызывается автоматически в конструкторе.
   */
  protected readStorage() {
    const items: ILocalStorageState = {};
    this.items.forEach((item) => {
      items[item] = tryParseJson(this.localStorage.getItem(item));
    });
    this.setState(items);
  }

  /**
   * Сохраняет в localStorage новое значение ключа, предварительно его сериализовав.
   * Также обновляет состояние стора.
   *
   * @param item - ключ
   * @param value - новон десериализованное значение
   */
  setItem(item: string, value: any) {
    this.localStorage.setItem(item, JSON.stringify(value));
    this.setState({ [item]: value });
  }

  /**
   * Возвращает обзервабл на значение переданного ключа
   * @param item - ключ
   */
  get$<T = any>(item: string): ObservableWithValue<T> {
    return attachValueToObservable(
      this.state$.pipe(
        map((state) => state[item]),
        distinctUntilChanged()
      ),
      () => this.state[item]
    );
  }

  /**
   * Обрабатывает событие window.storage
   * @param e
   */
  protected handleStorageChange = (e: StorageEvent) => {
    if (!e.key || this.items.indexOf(e.key) === -1) {
      return;
    }
    if (e.newValue === e.oldValue) {
      return;
    }

    const value = tryParseJson(e.newValue);
    this.setState({
      [e.key]: value
    });
  };
}
