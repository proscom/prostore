import { BehaviorStore, SubscriptionManager } from '@proscom/prostore';
import { IStorageValueAdapter } from './types';
import { switchMap } from 'rxjs/operators';
import { from } from 'rxjs';

export type StorageValueParser<T> = (
  value: any
) => T | null | Promise<T | null>;

export interface StorageValueState<T> {
  value: T | null;
  loaded: boolean;
}

/**
 * Стор, состояние которого синхронизировано с внешним хранилищем,
 * задаваемым с помощью adapter (например, localStorage)
 *
 * @example
 * const LOCAL_STORAGE_KEY_SCORE = 'score';
 *
 * // Адаптер для взаимодействия с localStorage
 * const adapter = new WebStorageAdapter(localStorage, LOCAL_STORAGE_KEY_SCORE);
 *
 * // Синхронизированный стор
 * const scoreStore = new StorageValueStore<number>(
 *   // Адаптер для конкретного значения
 *   adapter,
 *   // Трансформер значения. Так как внешнее хранилище может изменяться извне,
 *   // то нет гарантии что значение имеет определенный тип.
 *   // С помощью трансформера можно привести значение к нужному типу
 *   (value) => +value || 0
 * );
 *
 * // Регистрируем обработчик события storage
 * scoreStore.registerListener();
 *
 * // При изменении значения во внешнем хранилище, оно изменится и в сторе.
 * // Подписываемся на изменение значения как на любой обзервабл
 * scoreStore.state$.subscribe((score) => {
 *   // После первоначальной загрузки данных из внешнего хранилища loaded=true
 *   // score.value содержит непосредственно значение
 *   console.log(score.loaded, score.value);
 * });
 *
 * // Значение стора можно изменить, тогда оно меняется и во внешнем хранилище
 * scoreStore.setValue(5);
 *
 * // Отписаться от обновлений стора можно так
 * sub.unsubscribe();
 *
 * // Перед удалением scoreStore обработчик событий надо снять
 * scoreStore.destroy();
 *
 */
export class StorageValueStore<T = any> extends BehaviorStore<
  StorageValueState<T>
> {
  static initialState = { loaded: false, value: null };
  sub = new SubscriptionManager();

  /**
   * @param adapter - адаптер для внешнего хранилища
   * @param parseValue - трансформер для значения.
   *  Так как внешнее хранилище может изменяться извне,
   *  то нет гарантии что значение имеет определенный тип.
   *  С помощью трансформера можно привести значение к нужному типу
   */
  constructor(
    public adapter: IStorageValueAdapter,
    public parseValue: StorageValueParser<T> = (x) => x
  ) {
    super({
      ...StorageValueStore.initialState
    });
  }

  onSubscribe() {
    this.sub.subscribe(
      this.adapter.value$.pipe(
        switchMap((storageValue) =>
          from(Promise.resolve().then(() => this.parseValue(storageValue)))
        )
      ),
      this.handleStorageValueChange
    );
  }

  onUnsubscribe() {
    this.sub.destroy();
  }

  /**
   * @deprecated This method is no longer required
   */
  registerListener() {}

  /**
   * @deprecated This method is no longer required
   */
  destroy() {}

  handleStorageValueChange = (parsed: T | null) => {
    if (parsed !== this.state.value) {
      this.setState({ value: parsed, loaded: true });
    } else if (!this.state.loaded) {
      this.setState({ loaded: true });
    }
  };

  setValue(value: T | null | ((oldValue: T | null) => T | null)) {
    let newValue = value;
    if (typeof value === 'function') {
      const fn = value as (oldValue: T | null) => T | null;
      newValue = fn(this.state.value);
    }
    this.setState({ value: newValue as T | null });
    this.adapter.setValue(newValue);
  }
}
