import { Observable } from "rxjs";

export interface IStorageValueAdapter<T = any> {
  value$: Observable<T | null>;
  setValue(value: T | null): void;
}
