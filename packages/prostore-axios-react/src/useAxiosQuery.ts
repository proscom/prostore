import {DependencyList, useContext, useMemo} from 'react';
import { AxiosInstance } from 'axios';
import { PartialKey } from '@proscom/prostore';
import {
  AxiosQueryStore,
  IAxiosQueryOptions,
  IAxiosQueryStoreParams
} from '@proscom/prostore-axios';
import {
  ProstoreSsrContext,
  UseRequestOptions,
  useRequestStore
} from '@proscom/prostore-react';
import { useContextAxiosClient } from './AxiosClientsContext';

export type IUseAxiosQueryOptions<Vars, Data> = PartialKey<
  IAxiosQueryStoreParams<Vars, Data>,
  'client'
>;

export interface IUseAxiosQueryParams<Vars, Data> {
  /** Axios Client instance to be used for data fetching, or its name for it to be injected from the context */
  client?: string | AxiosInstance;
  /** Query specification */
  queryOptions?: IUseAxiosQueryOptions<Vars, Data>;
  /** Query variables */
  variables?: Vars;
  /** Additional query options */
  options?: IAxiosQueryOptions<Vars, Data>;
  /** Additional hook options */
  hookOptions?: UseRequestOptions<Vars, Data>;
}

/**
 * Creates AxiosQueryStore and subscribes to it using useRequestStore.
 * Effectively this requests data from the axios based on the provided variables and queryOptions
 *
 * @returns {{load, check, state}} - Current request state
 */
export function useAxiosQuery<Vars, Data>({
  client,
  queryOptions,
  variables,
  options,
  hookOptions
}: IUseAxiosQueryParams<Vars, Data>) {
  const actualClient = useContextAxiosClient(client);
  const ssrContext = useContext(ProstoreSsrContext);

  const store = useMemo(
    () =>
      new AxiosQueryStore<Vars, Data>({
        ssrContext,
        client: actualClient,
        ...queryOptions
      }),
    [ssrContext, actualClient, queryOptions]
  );

  return useRequestStore(store, variables, options, hookOptions);
}

export interface IUseAxiosQueryVarsParams<Vars, Data>
  extends IUseAxiosQueryParams<Vars, Data> {
  variableCreator: [() => Vars, DependencyList];
}

/**
 * The same as useAxiosQuery, but it uses variableCreator which is supplied to useMemo and
 * can be used to memoize query variables. This may be used when the default behavior of deeply
 * comparing variables is not preferable (i.e., it is not fast enough, or variables are not simple objects)
 *
 * @returns {{load, check, state}} - Current request state
 * @deprecated This function is not needed since useAxiosQuery performs deep equality check
 * on variables
 */
export function useAxiosQueryVars<Vars, Data>({
  variableCreator,
  ...queryProps
}: IUseAxiosQueryVarsParams<Vars, Data>) {
  const variables = useMemo(...variableCreator);
  return useAxiosQuery({ ...queryProps, variables });
}
