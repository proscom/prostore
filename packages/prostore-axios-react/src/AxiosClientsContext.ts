import { createContext, useContext } from 'react';
import {AxiosInstance} from 'axios';

export type IAxiosClients = {
  [name: string]: AxiosInstance;
};

/**
 * React context to hold AxiosClientManager instance
 */
export const AxiosClientsContext = createContext<AxiosClientsManager|null>(null);

/**
 * Used as the IoC container to inject axios client instances into components
 * based on their names
 */
export class AxiosClientsManager {
  constructor(public clients: IAxiosClients) {}

  getClient(key: string = 'default') {
    return this.clients[key];
  }
}

/**
 * Injects axios client from the context based on the provided name or
 * uses the provided instance. If called without arguments, uses the default
 * instance
 *
 * @param clientOrName - name of the axios client to be injected, or instance to be used
 */
export function useContextAxiosClient(
  clientOrName?: string | AxiosInstance | null | undefined
): AxiosInstance {
  const clientsContext = useContext(AxiosClientsContext);

  if (!clientOrName || typeof clientOrName === 'string') {
    if (!clientsContext) {
      throw new Error(
        `AxiosClientsContext is not provided`
      );
    }
    const client = !clientOrName ? clientsContext.getClient() : clientsContext.getClient(clientOrName);
    if (!client) {
      throw new Error(
        `Client '${clientOrName}' is not found in the AxiosClientsContext store`
      );
    }
    return client;
  }

  return clientOrName;
}
