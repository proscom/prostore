import { createContext } from 'react';
import { IProstoreSsrContext } from '@proscom/prostore';

/**
 * React context which holds ProstoreSsrManager instance
 */
export const ProstoreSsrContext = createContext<ProstoreSsrManager|null>(null);

export type IStates = {
  [name: string]: any;
};

export interface IProstoreSsrManagerParams {
  /** Whether the application is running on the server or in the browser */
  isServer?: boolean;
  /** Initial states of the stores. Usually provided in the browser when the SSR data is received */
  initialStates?: IStates;
}

/**
 * Globally manages the store states for them to be used in the SSR-enabled environments
 */
export class ProstoreSsrManager implements IProstoreSsrContext {
  /** Whether the application is running on the server or in the browser */
  public isServer: boolean = false;
  /** Store states identified by store's ssrId */
  public states: IStates = {};
  /** Array of registered promises to be awaited during the SSR */
  public promises: Set<Promise<any>> = new Set();
  /** Do nothing if this is true. setState will do nothing and getState will return null */
  public disabled: boolean = false;

  constructor({ isServer, initialStates }: IProstoreSsrManagerParams) {
    this.isServer = isServer || false;
    this.states = initialStates || this.states;
  }

  /**
   * Registers the promise from the store for it to be awaited during SSR
   *
   * @param promise - promise to be registered
   */
  registerPromise(promise: Promise<any>) {
    this.promises.add(promise);
    promise.then(() => {
      this.promises.delete(promise);
    });
  }

  /**
   * Checks if it has any registered promises
   */
  hasPromises(): boolean {
    return this.promises.size > 0;
  }

  /**
   * Awaits all current promises
   */
  waitForCurrentPromises(): Promise<any> {
    return Promise.all(this.promises);
  }

  /**
   * Returns the saved state for the store with the provided ssrId
   * @param key - ssrId of the store
   * @returns saved state for the provided store
   */
  getState(key: string): any {
    if (this.disabled) return null;
    return this.states[key];
  }

  /**
   * Saves the state of the store with the provided ssrId
   * @param key - ssrId of the store
   * @param value - state to be saved
   */
  setState(key: string, value: any) {
    if (this.disabled) return;
    this.states[key] = value;
  }

  /**
   * Returns all store states
   */
  getStates(): IStates {
    return this.states;
  }

  /**
   * Changes the disabled flag
   * @param value - new value of the flag
   */
  setDisabled(value: boolean) {
    this.disabled = value;
  }
}
