import { useContext, useEffect, useState } from 'react';
import {
  CheckRequestResult,
  checkRequestState,
  CheckRequestStateFn,
  GetRequestStoreData,
  GetRequestStoreOptions,
  GetRequestStoreState,
  GetRequestStoreVars,
  RequestStore
} from '@proscom/prostore';
import { useLatestCallbackRef } from '@proscom/ui-react';
import { useConnectStore } from './useConnectStore';
import { useContextStore } from './ProstoreContext';
import { ProstoreSsrContext } from './ProstoreSsrContext';

export interface UseRequestStoreResult<Store extends RequestStore> {
  check: CheckRequestResult;
  state: GetRequestStoreState<Store>;
  load: (
    options?: GetRequestStoreOptions<Store>
  ) => Promise<GetRequestStoreState<Store>>;
  store: Store;
}

export interface UseRequestOptions<Vars = any, Data = any> {
  checkRequest?: CheckRequestStateFn<Vars, Data>;
}

/**
 * Attaches component to the provided store and requests data based on the variables and options
 *
 * @param storeOrName - RequestStore to subscribe to, or its name for it to be injected from the context
 * @param variables - Request variables. If they change, request will be fired again
 * @param options - Additional request options. They are not observed
 * @param hookOptions - Additional hook options:
 *  - checkRequest - Optional function which may be used to override default behavior of determining
 *                   when to fire the request
 */
export function useRequestStore<Store extends RequestStore>(
  storeOrName: string | Store,
  variables: GetRequestStoreVars<Store> | undefined = undefined,
  options: GetRequestStoreOptions<Store> | undefined = undefined,
  hookOptions: UseRequestOptions<
    GetRequestStoreVars<Store>,
    GetRequestStoreData<Store>
  > = {}
): UseRequestStoreResult<Store> {
  const checkRequest = hookOptions.checkRequest || checkRequestState;

  // Inject store from context
  const store = useContextStore<Store>(storeOrName);

  // Local copy of store state
  const [state, setState] = useState<GetRequestStoreState<Store>>(store.state);

  // Subscribe to store changes
  useConnectStore(store, setState);

  // Check if we need to request data
  const queryCheck = checkRequest(state, variables);

  // Memoized callback to request data
  const loadData = useLatestCallbackRef(
    (newOptions: GetRequestStoreOptions<Store> | undefined = undefined) => {
      return store.loadData(variables, newOptions || options);
    }
  );

  // Request the data if it is should be requested
  useEffect(() => {
    if (queryCheck.request) {
      // Error should not happen here, because
      // it is handled inside RequestStore,
      // but we attach the catch listener just in case
      loadData().catch((e) => console.error(e));
    }
  });

  // Attach ssrContext
  const ssrContext = useContext(ProstoreSsrContext);

  // Perform request if server rendering
  if (store.ssrId && ssrContext && ssrContext.isServer && queryCheck.request) {
    loadData().catch((e) => console.error(e));
  }

  // Return stuff which may be useful in the component
  return { check: queryCheck, state, load: loadData, store };
}
