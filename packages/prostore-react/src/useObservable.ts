import { Observable } from 'rxjs';
import { useEffect, useLayoutEffect, useState } from "react";
import { ObservableWithValue } from "@proscom/prostore";
import { useLatestCallbackRef } from "@proscom/ui-react";

/**
 * Subscribe to Observable reactively.
 * Whenever callback changes, the observable is resubscribed to, which
 * may lead to additional callback calls with the same value, because
 * some observable types call the subscriber immediately when subscription occurs.
 *
 * @param obs$ - observable to subscribe to
 * @param setState - callback to which the observable value is passed
 */
export function useObservable<State = any>(
  obs$: Observable<State>|null|undefined,
  setState: (state: State) => void
) {
  // useLayoutEffect is used here in order to minimize probability
  // of possible emits between render and effect execution
  // so that latest emits are less likely to be lost in case obs$ is not replayed
  useLayoutEffect(() => {
    if (!obs$) {
      return;
    }
    const sub = obs$.subscribe((state: State) => {
      setState(state);
    });
    return () => {
      sub.unsubscribe();
    };
  }, [obs$, setState]);
}

/**
 * Subscribe to Observable with useState included.
 *
 * To ensure that no values are lost, make sure that `obs$` replays its latest value.
 * You can make observable to do so by piping it through `shareReplay` operator:
 * `obs$ = otherObs$.pipe(shareReplay())`
 *
 * @param obs$ - observable to subscribe to
 * @param initialState - initial value for the embedded state
 */
export function useObservableState<State = any>(
  obs$: Observable<State>|null|undefined,
  initialState: State
): State;
/**
 * Subscribe to Observable with useState included.
 * Until the subscriber is called, the state value is null (on the first render).
 *
 * To ensure that no values are lost, make sure that `obs$` replays its latest value.
 * You can make observable to do so by piping it through `shareReplay` operator:
 * `obs$ = otherObs$.pipe(shareReplay())`
 *
 * @param obs$ - observable to subscribe to
 */
export function useObservableState<State = any>(
  obs$: Observable<State>|null|undefined
): State | null;
/**
 * Subscribe to Observable with useState included
 *
 * To ensure that no values are lost, make sure that `obs$` replays its latest value.
 * You can make observable to do so by piping it through `shareReplay` operator:
 * `obs$ = otherObs$.pipe(shareReplay())`
 *
 * @param obs$ - observable to subscribe to
 * @param initialState - initial value for the embedded state
 */
export function useObservableState<State = any>(
  obs$: Observable<State>|null|undefined,
  initialState: State | null = null
): State | null {
  const [state, setState] = useState(initialState);
  useObservable(obs$, setState);
  return state;
}

/**
 * Subscribe to observable with value.
 *
 * To ensure that no values are lost, make sure that `obs$` replays its latest value.
 * You can make observable to do so by piping it through `shareReplay` operator:
 * `obs$ = otherObs$.pipe(shareReplay())`
 *
 * @param sub$ - observable with value to subscribe to
 */
export function useSubject<State = any>(sub$: ObservableWithValue<State>): State;
/**
 * Subscribe to observable with value.
 *
 * To ensure that no values are lost, make sure that `obs$` replays its latest value.
 * You can make observable to do so by piping it through `shareReplay` operator:
 * `obs$ = otherObs$.pipe(shareReplay())`
 *
 * @param sub$ - observable with value to subscribe to
 */
export function useSubject<State = any>(sub$: ObservableWithValue<State>|null|undefined): State|null;
/**
 * Subscribe to observable with value.
 *
 * To ensure that no values are lost, make sure that `obs$` replays its latest value.
 * You can make observable to do so by piping it through `shareReplay` operator:
 * `obs$ = otherObs$.pipe(shareReplay())`
 *
 * @param sub$ - observable with value to subscribe to
 */
export function useSubject<State = any>(sub$: ObservableWithValue<State>|null|undefined) {
  return useObservableState(sub$, sub$?.value ?? null);
}

/**
 * Subscribe to observable imperatively.
 * In contrast to simple useObservable, in this hook the observable is not
 * resubscribed to whenever the callback changes. This is more suitable
 * for subscribing to events instead of values.
 *
 * @param obs$ - observable to subscribe to
 * @param callback - callback which is called whenever the observable changes
 */
export function useObservableCallback<State = any>(obs$: Observable<State>|null|undefined, callback: (state: State) => void) {
  const callbackRef = useLatestCallbackRef(callback);
  useObservable(obs$, callbackRef);
}
