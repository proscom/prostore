import { BehaviorSubject, Observable } from 'rxjs';
import { useFactoryRef } from '@proscom/ui-react';
import { useEffect, useMemo } from 'react';
import { distinctUntilChanged } from 'rxjs/operators';
import shallowequal from 'shallowequal';

export function usePropsObservable<Props extends object>(
  props: Props
): Observable<Props>;
export function usePropsObservable<Props extends object, Result = Props>(
  props: Props,
  factory: (props$: Observable<Props>) => Observable<Result>,
  deps: any[]
): Observable<Result>;
/**
 * Converts component props or any other data tied to the React render cycle
 * into an rxjs observable.
 * This observable can then be used to construct rxjs pipelines and subscribe
 * component back to it.
 *
 * Note, that subscribing the same component to this observable returned from
 * this function may lead to infinite rerender cycles if props do not stabilize
 * during re-renders.
 * Props are checked with shallow equality to combat this in most cases.
 *
 * @param props - object which is converted to an observable so that
 *  each time any of the object fields change, observable will emit a new value
 * @param factory - function which gets observable from props and returns
 * @param deps - array of dependencies similar to useMemo, which would
 *  restrict `factory` from re-running on every render
 *
 * @example
 * const filteredList$ = usePropsObservable({ search: props.search }, (props$) => {
 *   return combineLatest(props$, list$).pipe(map((props, list) => {
 *     return list.filter(item => item.indexOf(search) >= 0);
 *   }));
 * }, []);
 *
 * const filteredList = useObservableState(filteredList$);
 */
export function usePropsObservable<Props extends object, Result = Props>(
  props: Props,
  factory?: (props$: Observable<Props>) => Observable<Result>,
  deps?: any[]
): Observable<Props | Result> {
  const props$ = useFactoryRef(() => new BehaviorSubject(props));
  const result$ = useMemo(() => {
    const base$ = props$.pipe(distinctUntilChanged(shallowequal));
    if (factory) {
      return factory(base$);
    }
    return base$;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props$, ...(deps || [])]);
  useEffect(() => {
    props$.next(props);
  }, [props$, props]);
  return result$;
}
