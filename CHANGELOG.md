## 0.2.4 (2021-02-05)

### `@proscom/prostore`

- Обновлена зависимость `@proscom/ui-utils` с версии `<0.1` до `<0.2`
- Добавлен новый класс `ValueStore`. См. README для примера использования
- Добавлено ограничение на аргумент дженерика в `BehaviorStore` и `AsyncBehaviorStore`.
  Теперь тип `State` должен расширять `object`.
  Это ограничение и так требуется для корректной работы сторов, но теперь оно проверяется также на уровне TypeScript.

### `@proscom/prostore-react`

- Обновлена зависимость `@proscom/ui-react` с версии `<0.1` до `<0.2`
- Добавлен хук `useObservableCallback` для более удобной подписки на обзерваблы без создания стейта

### `@proscom/prostore-local-storage`

- Обновлена зависимость `@proscom/ui-utils` с версии `<0.1` до `<0.2`

## 0.2.2 (2021-01-29)

### `@proscom/prostore`

- Добавлена новый интерфейс `ObservableWithValue<T>`, обозначающий обзервабл, у которого
  можно получить текущее значение.
- Добавлена функция-утилита `attachValueToObservable(obs$, valueFn)`, которая позволяет
  добавить к существующему обзерваблу функцию-геттер, возвращающую текущее значение.

### `@proscom/prostore-axios`

- В колбек `mapData` конструктора `AxiosQueryStore` теперь вторым аргументом передаётся полный AxiosResponse.

### `@proscom/prostore-axios-react`

- В колбек `mapData` в параметре `queryOptions` хука `useAxiosQuery` теперь вторым аргументом передаётся полный AxiosResponse.

### `@proscom/prostore-react`

- `useAsyncOperation` теперь выбрасывает ошибку при попытке повторного запуска операции в singleton-режиме,
  а не возвращает `undefined`. Соответствующие TypeScript типы больше не включают пустые значения.

- Хук `useSubject` теперь принимает не `BehaviorSubject`, а более общий тип `ObservableWithValue`.

- Добавлен новый хук `usePropsObservable`. См. README для описания и примера использования.

### `@proscom/prostore-react-router`

- В тип `QueryPart`, метод `LocationStore.get$` и хук `useLocationQuery` добавлен аргумент дженерика `Params`,
  который позволяет более строго типизировать десериализованные значения.
- Добавлены утилиты `defineQueryTransformers` и `ExtractTransformedQueryParams`.
  Их можно использовать для формирования типа, который можно передать в `Params` в соответствии
  с изменениями выше. См. пример в README.
- Метод `LocationStore.get$` теперь возвращает `ObservableWithValue`.

- Хук `useLocationQuery` теперь возвращает корректные значения даже при первом рендере.
  Соответствующие типы сделаны более строгими.
- Метод `LocationStore.createUrlPreserver` теперь может принимать `null` и `undefined`.

## 0.2.1 (2020-12-25)

### `@proscom/prostore-react`

- Обновлена зависимость `@proscom/ui-react` с версии `0.0.6` до версии `0.0.7`

## 0.2.0 (2020-12-25)

### `@proscom/prostore`

- Добавлена функция-утилита `insertPaginatedSlice`, которую можно использовать в качестве
  `updateData` при реализации пагинации с дозагрузкой. См. [README](./packages/prostore/README.md#Пагинация-с-дозагрузкой-в-RequestStore)
  для примера

### `@proscom/prostore-react`

- **Breaking change!** Хук `useRequestStore` теперь принимает один аргумент дженерика `Store`
  вместо старых трёх (`Vars`, `Data` и `Options`). Изменение касается только использования в TypeScript.
  Чтобы обновить код, замените вызовы `useRequestStore<Vars, Data, Options>()`, содержащие
  явные аргументы дженерика, вызовами `useRequestStore<Store>()` или
  `useRequestStore<RequestStore<Vars, Data, Options>>()`

- Добавлен новый хук `useAsyncOperation` для отслеживания статуса императивной асинхронной операции.
  Подробнее см. в [README](./packages/prostore-react/README.md#useAsyncOperation)

### `@proscom/prostore-axios`

- Добавлен параметр `mapVariables` в конструктор `AxiosQueryStore`. Он позволяет
  использовать в качестве `variables` произвольную структуру данных, преобразовав
  её в параметры `AxiosRequestConfig` непосредственно перед отправкой запроса.

  Для удобства добавлены статические колбеки `AxiosQueryStore.mapVariablesParams` и
  `AxiosQueryStore.mapVariablesData`, для частых случаев проброса GET-параметров и
  тела в формате JSON.

### `@proscom/prostore-axios-react`

- Добавлен параметр `mapVariables` для хука `useAxiosQuery`, соответствующий
  одноименному параметру `AxiosQueryStore`

### `@proscom/prostore-apollo-react`

- Добавлен хук `useGraphqlMutation` для отслеживания статуса GraphQL-мутации.
